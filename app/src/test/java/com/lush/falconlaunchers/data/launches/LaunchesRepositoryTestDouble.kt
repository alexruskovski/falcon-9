package com.lush.falconlaunchers.data.launches

import com.lush.falconlaunchers.data.api.ApiResponseWrapper
import com.lush.falconlaunchers.data.api.ApiSuccessResponse
import com.lush.falconlaunchers.model.Launch
import com.lush.falconlaunchers.model.Links
import com.lush.falconlaunchers.model.Patch

class LaunchesRepositoryTestDouble: LaunchesRepository{


    var returnError = false

    companion object{
        val LAUNCH_ID = "test"
    }

    override suspend fun getAllLaunches(): ApiResponseWrapper<List<Launch>> {
        return if(returnError){
            ApiResponseWrapper.create(Exception("Failed"))
        }else{
            ApiSuccessResponse(listOf(testLaunch))
        }
    }

    override suspend fun getSingleLaunchDetails(launchId: String): ApiResponseWrapper<Launch> {
        return if(returnError){
            ApiResponseWrapper.create(Exception("Failed"))
        }else{
            ApiSuccessResponse(testLaunch)
        }
    }

    private val testLaunch = Launch(
        id = LAUNCH_ID,
        name = "test",
        launchDateUTC = "launchDateUTC",
        links = Links(Patch("","")),
        details = "",
        success = false
    )

}