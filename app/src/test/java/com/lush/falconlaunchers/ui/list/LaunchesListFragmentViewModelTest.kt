package com.lush.falconlaunchers.ui.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.lush.falconlaunchers.data.launches.LaunchesRepositoryTestDouble
import com.lush.falconlaunchers.util.getOrAwaitValuesTest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LaunchesListFragmentViewModelTest {

    private lateinit var sut: LaunchesListFragmentViewModel
    private lateinit var launchesRepositoryTestDouble: LaunchesRepositoryTestDouble
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @get:Rule val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        launchesRepositoryTestDouble = LaunchesRepositoryTestDouble()
        sut = LaunchesListFragmentViewModel(launchesRepositoryTestDouble)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }


    @Test
    fun getAllLaunches_callFails_ErrorExpected(){
        launchesRepositoryTestDouble.returnError = true

        sut.getAllLaunches()
        val viewStates = sut.observeViewState().getOrAwaitValuesTest(2)

        //First value expected is LOADING
        assertThat(viewStates[0]).isNotNull()
        assertThat(viewStates[0]).isInstanceOf(LaunchesListFragmentViewState.Loading::class.java)
        //Second one should be ERROR
        assertThat(viewStates[1]).isNotNull()
        assertThat(viewStates[1]).isInstanceOf(LaunchesListFragmentViewState.Error::class.java)
    }

    @Test
    fun getAllLaunches_LaunchesReturned_ActualResponseExpected(){
        launchesRepositoryTestDouble.returnError = false

        sut.getAllLaunches()
        val viewStates = sut.observeViewState().getOrAwaitValuesTest(2)

        //First value expected is LOADING
        assertThat(viewStates[0]).isNotNull()
        assertThat(viewStates[0]).isInstanceOf(LaunchesListFragmentViewState.Loading::class.java)
        //Second one should be ERROR
        assertThat(viewStates[1]).isNotNull()
        assertThat(viewStates[1]).isInstanceOf(LaunchesListFragmentViewState.LaunchesList::class.java)
    }



}