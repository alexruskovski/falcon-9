package com.lush.falconlaunchers.ui.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.lush.falconlaunchers.data.api.ApiSuccessResponse
import com.lush.falconlaunchers.data.launches.LaunchesRepositoryTestDouble
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import com.google.common.truth.Truth.assertThat
import com.lush.falconlaunchers.util.getOrAwaitValueTest
import com.lush.falconlaunchers.util.getOrAwaitValuesTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class LaunchDetailsFragmentViewModelTest{
    private lateinit var sut: LaunchDetailsFragmentViewModel
    private lateinit var launchesRepositoryTestDouble: LaunchesRepositoryTestDouble
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        launchesRepositoryTestDouble = LaunchesRepositoryTestDouble()
        sut = LaunchDetailsFragmentViewModel(launchesRepositoryTestDouble)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun getAllLaunches_callFails_ErrorExpected(){
        launchesRepositoryTestDouble.returnError = true

        sut.getLaunchDetails(LaunchesRepositoryTestDouble.LAUNCH_ID)
        val viewStates = sut.observeViewState().getOrAwaitValuesTest(2)

        //First value expected is LOADING
        assertThat(viewStates[0]).isNotNull()
        assertThat(viewStates[0]).isInstanceOf(LaunchDetailsFragmentViewState.Loading::class.java)
        //Second one should be ERROR
        assertThat(viewStates[1]).isNotNull()
        assertThat(viewStates[1]).isInstanceOf(LaunchDetailsFragmentViewState.Error::class.java)
    }

    @Test
    fun getAllLaunches_LaunchesReturned_ActualResponseExpected(){
        launchesRepositoryTestDouble.returnError = false

        sut.getLaunchDetails(LaunchesRepositoryTestDouble.LAUNCH_ID)
        val viewStates = sut.observeViewState().getOrAwaitValuesTest(2)

        //First value expected is LOADING
        assertThat(viewStates[0]).isNotNull()
        assertThat(viewStates[0]).isInstanceOf(LaunchDetailsFragmentViewState.Loading::class.java)
        //Second one should be ERROR
        assertThat(viewStates[1]).isNotNull()
        assertThat(viewStates[1]).isInstanceOf(LaunchDetailsFragmentViewState.LaunchDetails::class.java)
    }


}

