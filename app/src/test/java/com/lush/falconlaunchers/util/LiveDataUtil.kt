package com.lush.falconlaunchers.util

import androidx.lifecycle.LiveData
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import androidx.lifecycle.Observer as Observer1

/* Copyright 2019 Google LLC.
   SPDX-License-Identifier: Apache-2.0 */

fun <T> LiveData<T>.getOrAwaitValueTest(
        time: Long = 2,
        timeUnit: TimeUnit = TimeUnit.SECONDS
): T {
    var data: T? = null
    val latch = CountDownLatch(1)
    val observer = object : Observer1<T> {
        override fun onChanged(o: T?) {
            data = o
            latch.countDown()
            this@getOrAwaitValueTest.removeObserver(this)
        }
    }

    this.observeForever(observer)

    // Don't wait indefinitely if the LiveData is not set.
    if (!latch.await(time, timeUnit)) {
        throw TimeoutException("LiveData value was never set.")
    }

    @Suppress("UNCHECKED_CAST")
    return data as T
}



fun <T> LiveData<T>.getOrAwaitValuesTest(
    waitTillNumberOfChanges: Int = 1
): List<T?>{
    val data = mutableListOf<T?>()
    val latch = CountDownLatch(waitTillNumberOfChanges)
    val observer = object : Observer1<T> {
        override fun onChanged(o: T?) {
            data.add(o)
            latch.countDown()
            if(latch.count == 0.toLong())
                this@getOrAwaitValuesTest.removeObserver(this)
        }
    }

    this.observeForever(observer)

    // Don't wait indefinitely if the LiveData is not set. Max 20sec
    if (!latch.await(20, TimeUnit.SECONDS)) {
        throw TimeoutException("LiveData value was never set.")
    }

    @Suppress("UNCHECKED_CAST")
    return data.toList()
}