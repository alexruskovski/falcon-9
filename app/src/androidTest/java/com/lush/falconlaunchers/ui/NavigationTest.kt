package com.lush.falconlaunchers.ui

import androidx.test.espresso.Espresso
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.lush.falconlaunchers.ui.list.LaunchesAdapter
import org.junit.Rule
import org.junit.runner.RunWith
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.lush.falconlaunchers.R
import com.lush.falconlaunchers.util.TrackingIdleResource
import org.junit.After
import org.junit.Before
import org.junit.Test

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class NavigationTest{

    //using ActivityTestRule even though is deprecated
    // as there is an issue with AndroidX ActivityScenario
    @get:Rule
    val mActivityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setup(){
        IdlingRegistry.getInstance().register(
            TrackingIdleResource.countingIdlingResource
        )
    }

    @After
    fun tearDown(){
        IdlingRegistry.getInstance().unregister(TrackingIdleResource.countingIdlingResource)
    }

    @Test
    fun testListToDetails(){
        onView(withId(R.id.rvLaunches))
            .perform(RecyclerViewActions.actionOnItemAtPosition<LaunchesAdapter.LaunchViewHolder>(3, click()))
        //VERIFY THAT THE DETAILS FRAG IS DISPLAYED
        onView(withId(R.id.launchDetailsFragmentRoot))
            .check(ViewAssertions.matches(isDisplayed()))
    }

    @Test
    fun testListToDetailsBackToList(){
        onView(withId(R.id.rvLaunches))
            .perform(RecyclerViewActions.actionOnItemAtPosition<LaunchesAdapter.LaunchViewHolder>(3, click()))
        //VERIFY THAT THE DETAILS FRAG IS DISPLAYED
        onView(withId(R.id.launchDetailsFragmentRoot))
            .check(ViewAssertions.matches(isDisplayed()))

        Espresso.pressBack()

        //VERIFY THAT THE LIST FRAG IS DISPLAYED AGAIN
        onView(withId(R.id.launches_list_fragment_root))
            .check(ViewAssertions.matches(isDisplayed()))

    }

}