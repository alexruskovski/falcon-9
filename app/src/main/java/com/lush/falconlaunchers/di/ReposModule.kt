package com.lush.falconlaunchers.di

import com.lush.falconlaunchers.data.api.usecases.UCGetLaunches
import com.lush.falconlaunchers.data.api.usecases.UCGetSingleLaunchDetails
import com.lush.falconlaunchers.data.launches.LaunchesRepository
import com.lush.falconlaunchers.data.launches.LaunchesRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

@InstallIn(SingletonComponent::class)
@Module
class ReposModule {

    @Provides
    @Singleton
    fun providesLaunchesRepository(
        ucGetLaunches: UCGetLaunches,
        ucGetSingleLaunchDetails: UCGetSingleLaunchDetails
    ):LaunchesRepository{
        return LaunchesRepositoryImpl(ucGetLaunches, ucGetSingleLaunchDetails)
    }

}