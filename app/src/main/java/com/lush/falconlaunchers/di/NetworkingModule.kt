package com.lush.falconlaunchers.di

import android.app.Application
import android.util.Log
import com.lush.falconlaunchers.BuildConfig
import com.lush.falconlaunchers.Constants
import com.lush.falconlaunchers.data.api.Api
import com.squareup.moshi.Moshi
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

@InstallIn(SingletonComponent::class)
@Module
class NetworkingModule {

    @Singleton
    @Provides
    fun providesRetrofit(okHttp: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.API_ENDPOINT)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(okHttp)
            .build()
    }

    @Singleton
    @Provides
    fun providesMoshi(): Moshi =
        Moshi.Builder()
            .build()

    @Provides
    @Singleton
    fun api(retrofit: Retrofit): Api = retrofit.create(Api::class.java)

    @Provides
    fun providesOKHTTP(
        application: Application
    ): OkHttpClient {
        val cacheSize: Long = 64 * 1024 * 1024 // 64 MBs
        val cache = Cache(application.cacheDir, cacheSize)
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
        }
        return builder.cache(cache)
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun providesPicasso(application: Application, okHttp: OkHttpClient): Picasso {
        val picassoListener =
            Picasso.Listener { _, uri, exception ->
                Log.e(
                    "picasso_failed",
                    "Picasso failed to load ${uri}. Reason: ${exception?.localizedMessage ?: ""}"
                )
            }
        return Picasso.Builder(application)
            .downloader(OkHttp3Downloader(okHttp))
            .listener(picassoListener)
            .build()
    }

}