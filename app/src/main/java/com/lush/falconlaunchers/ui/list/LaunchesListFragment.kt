package com.lush.falconlaunchers.ui.list

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.doOnPreDraw
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.lush.falconlaunchers.R
import com.lush.falconlaunchers.databinding.LaunchesListFragmentBinding
import com.lush.falconlaunchers.model.Launch
import com.lush.falconlaunchers.util.TrackingIdleResource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

@AndroidEntryPoint
class LaunchesListFragment: Fragment(R.layout.launches_list_fragment) {

    private lateinit var binding: LaunchesListFragmentBinding
    private val viewModel: LaunchesListFragmentViewModel by viewModels()

    @Inject lateinit var launchesAdapter: LaunchesAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        TrackingIdleResource.increment()

        binding = LaunchesListFragmentBinding.bind(view)

        postponeEnterTransition()
        binding.rvLaunches.doOnPreDraw { startPostponedEnterTransition() }

        viewModel.observeViewState()
            .observe(viewLifecycleOwner, this::render)

        setRecyclerView()
        setSwipeToRefresh()
    }



    override fun onResume() {
        super.onResume()
        viewModel.getAllLaunches()
    }

    private fun setSwipeToRefresh(){
        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.getAllLaunches()
        }
    }

    private fun setRecyclerView(){
        binding.rvLaunches.adapter = launchesAdapter
    }

    private fun initLaunchesList(launches_: List<Launch>?){
        TrackingIdleResource.decrement()
        if(launchesAdapter.itemCount > 0) return
        val launches = launches_ ?: return
        binding.rvLaunches.layoutAnimation = AnimationUtils.loadLayoutAnimation(
            requireContext(),
            R.anim.slide_down_layoutanimation
        )
        launchesAdapter.init(launches)
        launchesAdapter.setOnLaunchClicked(this::onLaunchClicked)

    }

    private fun onLaunchClicked(ivIcon: ImageView, launchId: String){
        val extras = FragmentNavigatorExtras(
            ivIcon to launchId
        )
        val action = LaunchesListFragmentDirections.actionLaunchesListFragmentToLaunchDetailsFragment(launchId)
        findNavController()
            .navigate(action, extras)
    }

    private fun onErrorThrown(msg: String){
        binding.tvErrorMessage.text = requireContext().getString(R.string.launches_list_fragment_no_internet_err_msg, msg)
        binding.tvErrorMessage.visibility = View.VISIBLE
        binding.ivFalconLogo.visibility = View.VISIBLE
        binding.ivFalconLogo.setOnClickListener {
            viewModel.getAllLaunches()
        }
    }


    private fun render(viewState: LaunchesListFragmentViewState){
        binding.tvErrorMessage.visibility = View.GONE
        binding.ivFalconLogo.visibility = View.GONE

        if(viewState is LaunchesListFragmentViewState.Loading){
            binding.swipeRefreshLayout.isRefreshing = true
            binding.rvLaunches.isVisible = false
        }else{
            binding.swipeRefreshLayout.isRefreshing = false
            binding.rvLaunches.isVisible = true
        }

        when(viewState){
            is LaunchesListFragmentViewState.Error -> onErrorThrown(viewState.errorMessage)
            is LaunchesListFragmentViewState.LaunchesList ->
                initLaunchesList(viewState.launches)
        }

    }

}