package com.lush.falconlaunchers.ui.details

import com.lush.falconlaunchers.model.Launch
import com.lush.falconlaunchers.ui.list.LaunchesListFragmentViewState


/**
 * Created by Alexander Ruskovski on 01/07/2021
 */


sealed class LaunchDetailsFragmentViewState {
    object Loading: LaunchDetailsFragmentViewState()
    object EmptyResponse: LaunchDetailsFragmentViewState()
    data class LaunchDetails(val launch: Launch): LaunchDetailsFragmentViewState()
    data class Error(val errorMessage: String): LaunchDetailsFragmentViewState()
}