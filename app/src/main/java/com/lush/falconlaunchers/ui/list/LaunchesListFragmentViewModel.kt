package com.lush.falconlaunchers.ui.list

import androidx.lifecycle.*
import com.lush.falconlaunchers.data.api.ApiEmptyResponse
import com.lush.falconlaunchers.data.api.ApiErrorResponse
import com.lush.falconlaunchers.data.api.ApiSuccessResponse
import com.lush.falconlaunchers.data.launches.LaunchesRepository
import com.lush.falconlaunchers.model.Launch
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

@HiltViewModel
class LaunchesListFragmentViewModel @Inject constructor(
    private val launchesRepository: LaunchesRepository
): ViewModel() {

    private val viewState = MutableLiveData<LaunchesListFragmentViewState>()
    private val cachedLaunches = mutableListOf<Launch>()

    fun observeViewState(): LiveData<LaunchesListFragmentViewState> =
        viewState

    fun getAllLaunches() = viewModelScope.launch{

        if(cachedLaunches.isNotEmpty()){
            viewState.postValue(LaunchesListFragmentViewState.LaunchesList(cachedLaunches))
            return@launch
        }
        viewState.postValue(LaunchesListFragmentViewState.Loading)
        when(val response = launchesRepository.getAllLaunches()){
            is ApiEmptyResponse ->
                viewState.postValue(LaunchesListFragmentViewState.LaunchesList(emptyList()))
            is ApiErrorResponse ->
                viewState.postValue(LaunchesListFragmentViewState.Error(response.errorMessage))
            is ApiSuccessResponse -> {
                cachedLaunches.clear()
                cachedLaunches.addAll(response.body)
                viewState.postValue(LaunchesListFragmentViewState.LaunchesList(cachedLaunches))
            }
        }
    }

}