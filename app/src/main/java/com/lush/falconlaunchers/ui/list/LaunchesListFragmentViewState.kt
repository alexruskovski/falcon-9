package com.lush.falconlaunchers.ui.list

import com.lush.falconlaunchers.model.Launch


/**
 * Created by Alexander Ruskovski on 30/06/2021
 */


sealed class LaunchesListFragmentViewState {
    object Loading: LaunchesListFragmentViewState()
    data class LaunchesList(val launches: List<Launch>): LaunchesListFragmentViewState()
    data class Error(val errorMessage: String): LaunchesListFragmentViewState()
}