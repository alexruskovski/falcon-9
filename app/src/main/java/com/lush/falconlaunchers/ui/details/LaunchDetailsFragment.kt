package com.lush.falconlaunchers.ui.details

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.lush.falconlaunchers.R
import com.lush.falconlaunchers.databinding.LaunchDetailsFragmentBinding
import androidx.navigation.fragment.navArgs
import com.lush.falconlaunchers.Constants
import com.lush.falconlaunchers.model.Launch
import com.lush.falconlaunchers.util.TrackingIdleResource
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import javax.inject.Inject


/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

@AndroidEntryPoint
class LaunchDetailsFragment: Fragment(R.layout.launch_details_fragment) {

    private lateinit var binding: LaunchDetailsFragmentBinding
    private val viewModel: LaunchDetailsFragmentViewModel by viewModels()
    private val args: LaunchDetailsFragmentArgs by navArgs()

    @Inject lateinit var picasso: Picasso

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TrackingIdleResource.increment()
        val animation = TransitionInflater.from(requireContext()).inflateTransition(
            android.R.transition.move
        )
        sharedElementEnterTransition = animation
        sharedElementReturnTransition = animation
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = LaunchDetailsFragmentBinding.bind(view)

        viewModel.observeViewState().observe(viewLifecycleOwner, this::render)

        postponeEnterTransition()

        viewModel.getLaunchDetails(args.launchId)

        setToolbar()
        setOnBackPressed()
    }

    private fun setOnBackPressed(){
        val onBackPressedCallback = object: OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                findNavController().navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            onBackPressedCallback
        )
    }

    private fun setToolbar() {
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun loadLaunchDetails(launch: Launch){
        TrackingIdleResource.decrement()
        binding.ivLaunchPatch.transitionName = launch.id
        launch.links.patch.large?.let {
            picasso.load(it).into(binding.ivLaunchPatch)
        }
        binding.ivLaunchPatch.doOnPreDraw { startPostponedEnterTransition() }

        binding.tvMissionName.text = launch.name
        binding.tvMissionLaunchedOn.text =
            DateTimeFormat.forPattern(Constants.LAUNCH_LIST_DATE_FORMAT).print(DateTime(launch.launchDateUTC))
        setLaunchStatus(launch.success)
        launch.details?.let {
            binding.tvMissionDetails.text = it
        }
    }

    private fun setLaunchStatus(success: Boolean?) {
        when (success) {
            true ->
                binding.ivMissionStatus.setImageResource(R.drawable.ic_launch_successful_24)
            false ->
                binding.ivMissionStatus.setImageResource(R.drawable.ic_launch_failed_24)
            else -> { //basically when status is null
                binding.ivMissionStatus.setImageResource(R.drawable.ic_launch_failed_24)
                binding.tvMissionSuccess.text = requireContext().getString(R.string.launches_list_fragment_list_mission_unknown_status)
            }
        }
    }

    private fun failedToLoadLaunchData(errMsg: String){
        val alertDialogBuilder = AlertDialog.Builder(requireContext())
        alertDialogBuilder.setMessage(requireContext().getString(R.string.launch_details_fragment_failed_to_load_launch_details, errMsg))
        alertDialogBuilder.setCancelable(false)
        alertDialogBuilder.setPositiveButton("OK"){d, _->
            d.dismiss()
            requireActivity().onBackPressed()
        }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.window?.setBackgroundDrawableResource(R.drawable.dialog_rounded_corners)
        alertDialog.window?.attributes?.windowAnimations = R.style.AlertDialogFadeInFadeOut
        alertDialog.show()
    }

    private fun render(viewState: LaunchDetailsFragmentViewState){
        when(viewState){
            is LaunchDetailsFragmentViewState.Error -> {
                startPostponedEnterTransition()
                failedToLoadLaunchData(viewState.errorMessage)
            }
            is LaunchDetailsFragmentViewState.LaunchDetails ->
                loadLaunchDetails(viewState.launch)
        }
    }
}