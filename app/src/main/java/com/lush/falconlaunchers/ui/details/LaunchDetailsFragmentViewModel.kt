package com.lush.falconlaunchers.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lush.falconlaunchers.data.api.ApiEmptyResponse
import com.lush.falconlaunchers.data.api.ApiErrorResponse
import com.lush.falconlaunchers.data.api.ApiSuccessResponse
import com.lush.falconlaunchers.data.launches.LaunchesRepository
import com.lush.falconlaunchers.ui.list.LaunchesListFragmentViewState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

@HiltViewModel
class LaunchDetailsFragmentViewModel @Inject constructor(
    private val launchesRepository: LaunchesRepository
): ViewModel() {

    private val viewState = MutableLiveData<LaunchDetailsFragmentViewState>()

    fun observeViewState(): LiveData<LaunchDetailsFragmentViewState> =
        viewState

    fun getLaunchDetails(launchId: String) = viewModelScope.launch {
        viewState.postValue(LaunchDetailsFragmentViewState.Loading)
        when(val response = launchesRepository.getSingleLaunchDetails(launchId)){
            is ApiEmptyResponse ->
                viewState.postValue(LaunchDetailsFragmentViewState.EmptyResponse)
            is ApiErrorResponse ->
                viewState.postValue(LaunchDetailsFragmentViewState.Error(response.errorMessage))
            is ApiSuccessResponse ->
                viewState.postValue(LaunchDetailsFragmentViewState.LaunchDetails(response.body))
        }
    }


}