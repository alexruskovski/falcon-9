package com.lush.falconlaunchers.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lush.falconlaunchers.R
import com.lush.falconlaunchers.databinding.LaunchRvItemBinding
import com.lush.falconlaunchers.model.Launch
import com.squareup.picasso.Picasso
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

@Singleton
class LaunchesAdapter @Inject constructor(
    private val picasso: Picasso
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val data = mutableListOf<Launch>()
    private var onLaunchClicked: (ivIcon: ImageView, String) -> Unit = {_,_ ->}

    fun init(newData: List<Launch>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    fun setOnLaunchClicked(onLaunchClicked: (ImageView, String) -> Unit) {
        this.onLaunchClicked = onLaunchClicked
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val launchRvItemBinding = LaunchRvItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return LaunchViewHolder(launchRvItemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        //just in case we add some more VH's in future
        when (holder) {
            is LaunchViewHolder -> holder.bind(data[position])
        }
    }

    override fun getItemCount() = data.size

    inner class LaunchViewHolder(
        private val binding: LaunchRvItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(launch: Launch) {
            binding.root.setOnClickListener { onLaunchClicked.invoke(binding.ivMissionPatch, launch.id) }
            binding.tvLaunchName.text = launch.name
            binding.tvLaunchDate.text = launch.getFormattedDate()
            invalidateLaunchStatus(launch.success)
            launch.links.patch.large?.let {
                picasso.load(it)
                    .placeholder(R.drawable.picasso_placeholder)
                    .into(binding.ivMissionPatch)
                binding.ivMissionPatch.transitionName = launch.id
            }
        }

        private fun invalidateLaunchStatus(isSuccessful: Boolean?) {
            val context = binding.root.context
            val missionStatusText = isSuccessful?.let {
                if (it)
                    context.getString(R.string.launches_list_fragment_list_mission_success)
                else
                    context.getString(R.string.launches_list_fragment_list_mission_success)
            } ?: context.getString(R.string.launches_list_fragment_list_mission_unknown_status)

            val missionStatusDrawable = isSuccessful?.let {
                if (it)
                    ContextCompat.getDrawable(context, R.drawable.ic_launch_successful_24)
                else
                    ContextCompat.getDrawable(context, R.drawable.ic_launch_failed_24)
            } ?: ContextCompat.getDrawable(context, R.drawable.ic_launch_failed_24)
            binding.tvMissionsStatus.text = missionStatusText
            binding.ivMissionStatus.setImageDrawable(missionStatusDrawable)
        }
    }


}