package com.lush.falconlaunchers.data.api.usecases

import com.lush.falconlaunchers.data.api.Api
import com.lush.falconlaunchers.data.api.ApiBase
import com.lush.falconlaunchers.data.api.ApiResponseWrapper
import com.lush.falconlaunchers.model.Launch
import com.squareup.moshi.JsonClass
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

@Singleton
class UCGetSingleLaunchDetails @Inject constructor(
    private val api: Api
): ApiBase<UCGetSingleLaunchDetails.Request, Launch>() {

    override suspend fun requestAsync(request: Request): ApiResponseWrapper<Launch> {
        val launchId = request.launchId
        return ApiResponseWrapper.create(api.getSingleLaunch(launchId))
    }

    @JsonClass(generateAdapter = true)
    data class Request(val launchId: String)

}