package com.lush.falconlaunchers.data.launches

import com.lush.falconlaunchers.data.api.ApiEmptyResponse
import com.lush.falconlaunchers.data.api.ApiErrorResponse
import com.lush.falconlaunchers.data.api.ApiResponseWrapper
import com.lush.falconlaunchers.data.api.ApiSuccessResponse
import com.lush.falconlaunchers.data.api.usecases.UCGetLaunches
import com.lush.falconlaunchers.data.api.usecases.UCGetSingleLaunchDetails
import com.lush.falconlaunchers.model.Launch


/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

class LaunchesRepositoryImpl(
    private val ucGetLaunches: UCGetLaunches,
    private val ucGetSingleLaunchDetails: UCGetSingleLaunchDetails
) : LaunchesRepository {


    override suspend fun getAllLaunches(): ApiResponseWrapper<List<Launch>> {
        return try{
            ucGetLaunches.requestAsync(Unit)
        }catch (ex: Exception) {
            ApiErrorResponse(ex.localizedMessage ?: "Unknown Error")
        }
    }

    override suspend fun getSingleLaunchDetails(launchId: String): ApiResponseWrapper<Launch> {
        return try {
            val request = UCGetSingleLaunchDetails.Request(launchId)
            ucGetSingleLaunchDetails.requestAsync(request)
        }catch (ex: Exception){
            ApiErrorResponse(ex.localizedMessage ?: "Unknown Error")
        }
    }
}