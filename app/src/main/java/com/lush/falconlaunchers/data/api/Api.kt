package com.lush.falconlaunchers.data.api

import com.lush.falconlaunchers.Constants
import com.lush.falconlaunchers.model.Launch
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

interface Api {

    @GET(Constants.GET_LATEST_LAUNCHES)
    suspend fun getAllLaunches(): Response<List<Launch>>

    @GET(Constants.GET_SPECIFIC_LAUNCH)
    suspend fun getSingleLaunch(
        @Path("id") launchId: String
    ): Response<Launch>

}