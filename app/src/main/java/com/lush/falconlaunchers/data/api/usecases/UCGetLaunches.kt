package com.lush.falconlaunchers.data.api.usecases

import com.lush.falconlaunchers.data.api.Api
import com.lush.falconlaunchers.data.api.ApiBase
import com.lush.falconlaunchers.data.api.ApiResponseWrapper
import com.lush.falconlaunchers.model.Launch
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

@Singleton
class UCGetLaunches @Inject constructor(
    private val api: Api
): ApiBase<Unit, List<Launch>>() {

    override suspend fun requestAsync(request: Unit): ApiResponseWrapper<List<Launch>> {
        return ApiResponseWrapper.create(api.getAllLaunches())
    }
}