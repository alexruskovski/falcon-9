package com.lush.falconlaunchers.data.launches

import com.lush.falconlaunchers.data.api.ApiResponseWrapper
import com.lush.falconlaunchers.model.Launch

interface LaunchesRepository {

    suspend fun getAllLaunches(): ApiResponseWrapper<List<Launch>>
    suspend fun getSingleLaunchDetails(launchId: String): ApiResponseWrapper<Launch>
}