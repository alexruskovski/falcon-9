package com.lush.falconlaunchers

object Constants {

    /** LAUNCH LIST */
    const val LAUNCH_LIST_DATE_FORMAT = "dd-MM-yyyy"


    /** API */
    const val API_VERSION = "v4"
    const val API_ENDPOINT = "https://api.spacexdata.com/$API_VERSION/"

    const val GET_LATEST_LAUNCHES = "launches/"
    const val GET_SPECIFIC_LAUNCH = "launches/{id}"
}