package com.lush.falconlaunchers

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


/**
 * Created by Alexander Ruskovski on 28/06/2021
 */

@HiltAndroidApp
class FalconApp : Application() {
}